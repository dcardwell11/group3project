let data = [];
let basket = [];
let currentSection = null;
let currentProduct = null;
let currentSectionNo = 0;
const state = {
    sectionOpen: null,
    productOpen: null,
    basketOpen: null,
};

var userID;

if (localStorage.getItem("ID") == null) {
    let userID = uuidv4();

    function uuidv4() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
            /[xy]/g,
            function(c) {
                var r = (Math.random() * 16) | 0,
                    v = c == "x" ? r : (r & 0x3) | 0x8;
                return v.toString(16);
            }
        );
    }
    localStorage.setItem("ID", userID);

    const mapping = "/baskets"
    const id = userID

    fetch(mapping, {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({ id })
    }).then(res => res.json()).then(getBasket).then(getSections).then(render).catch(console.error);
    window.location.reload(true)
} else {
    userID = localStorage.getItem("ID")
    getBasket().then(getSections).then(render);
}



function getSections() {
    return fetch("./sections")
        .then((Response) => Response.json())
        .then((_data) => {
            data = _data;
        })
        .catch((err) => console.error(err));

}

function getBasket() {
    return fetch(`/baskets/${userID}`)
        .then((Response) => Response.json())
        .then((_basket) => {
            basket = _basket;
        })
        .catch((err) => console.error(err));
}


function render() {
    let header = ` 
      <div class="header_container">
        <div class="header">
          <div onclick="closeAll();" class="header-logo" style="background-image: url(./g3logo.jpg); background-size: contain;background-position: center; background-repeat: no-repeat;"></div>
          <div style="background-image: url(./wordlogo.jpg); background-size: contain;background-position: left; background-repeat: no-repeat;"></div>
          <div style="text-align: right; ">
            <i onclick="closeAll();" class="fa fa-home" aria-hidden="true"></i>
            <a href="./admin.html"><i  class="fa fa-user" style="color: #5e879d"></i></a>
            <i class="fas fa-shopping-basket" onclick="displayBasket(); getBasket()"></i>
          </div>
        </div>
      </div>

      <header>
        <div class="nav">
          <ul id="ul">
          </ul>
        </div>
      </header>
      `;

    const headerEl = document.getElementById("main");
    headerEl.innerHTML = header;

    let li = data
        .map((sectionData, i) => {
            return `
  <li onclick="displayProducts(${i})"><a>${sectionData.name}</a></li>
      `;
        })
        .join("");

    const liEl = document.getElementById("ul");
    liEl.innerHTML = li;

    let content = `
    <linkSection id="links">

    ${data
      .map((sectionData, i) => {
        return `
            <item id="sections" onclick="displayProducts(${i})">              
                <div class="sectionsImage align item center" style="background-image: url(${sectionData.imageURL} );">
                
                </div>
                <div class="sectionsInfo">
                <a>${sectionData.name}</a>
              </div>
            </item>
            `;
      })
      .join("")}

    </linkSection>
    `;

  const linksEl = document.getElementById("load");
  linksEl.innerHTML = content;

  if (state.basketOpen) {
    let elbasket = `
    <section class="modal_Basket-bg">
          <div class="modal_Basket-box">
              <div class="modal_Basket-boxHeader">Your Basket<div style="text-align: right;"><i class='far fa-times-circle'  onclick="closeBasket()"></i></div></div>

                <div class="Modal_Basket-body">
                  <table>
                      <tr>
                          <th style="width: 10%;">ItemIMG</th>
                          <th style="width: 40%;">Item</th>
                          <th style="width: 8%;">Qty</th>
                          <th style="width: 10%;text-align: center;" >Price</th>
                      </tr>
                      <tr>
                     ${state.basketOpen.map((products, i) => {
                       return`
                          <td style="width: 10%; background-image: url(${products.imageURL}); background-size: contain; background-repeat: no-repeat; background-position: center;"></th>
                          <td style="width: 40%;">${products.name} - ${products.description}</th>
                          <td style="width: 8%;">1</th>
                          <td style="width: 10%;text-align: center;">£${products.price.toFixed(2)}</th>
                          <td style="width: 8%;"><span class="text" onclick="removeFromBasket(${i})">Remove</span></td>

                          </tr>`
                      }) .join("")}
                      
                  </table>
                </div>  
              <div class="checkout">
                <div class="label" >VAT:<div style="text-align: left;">£${(((basket.totalPrice)/120)*20).toFixed(2)}</div></div>
                <div class="label" >Total:<div style="text-align: left;font-weight: bold;">£${basket.totalPrice.toFixed(2)}</div></div>
                <div class="checkout-button" style="border-style: solid; text-align: center; padding: 9px;" onclick="confirm();checkout() ">CHECKOUT</div>
              </div>               
          </div>
         
      </section>
      
    `;

    const basketEl = document.getElementById("basket");
    basketEl.innerHTML = elbasket;
  } else {
    const basketEl = document.getElementById("basket");
    basketEl.innerHTML = "";
  }

  if (state.sectionOpen) {
    let sectionOn = `
  <div class="modal_page-bgc">
    <div class="modal_page-bg">
      <div class="modal_page-area" style=""height: 100%; width: 100%;>
            
            <div class="header_container">
            <div class="header">
                <div onclick="closeAll();" class="header-logo" style="background-image: url(./g3logo.jpg); background-size: contain;background-position: center; background-repeat: no-repeat;"></div>
                <div style="background-image: url(./wordlogo.jpg); background-size: contain;background-position: left; background-repeat: no-repeat;"></div>
                <div style="text-align: right; ">
                    <i onclick="closeAll();" class="fa fa-home" aria-hidden="true"></i>
                    <a href="./admin.html"><i  class="fa fa-user" style="color: #5e879d;"></i></a>
                    <i class="fas fa-shopping-basket" onclick="displayBasket(); getBasket()"></i>
                </div>
            </div>
        </div>

        <header>
            <div class="nav">
                <ul id="ul">
                ${data
                  .map((sectionData, i) => {
                    return `
            <li onclick="displayProducts(${i})"><a>${sectionData.name}</a></li>
                `;
                  })
                  .join("")}
                </ul>
            </div>
        </header>

        <div class="modal_page-image"></div>
        <div class="modal_page-title">
          <div style="font-size: xx-large;">${currentSection.name}</div>          
          <div class="dropdown">
            <button class="dropbtn">Sort:     ▾</button>
              <div class="dropdown-content">
                <a onclick="sortProductsAZ()">A - Z</a>
                <a onclick="sortProductsZA()">Z - A</a>
                <a onclick="sortProductsUP();">Price ▴</a>
                <a onclick="sortProductsDW()">Price ▾</a>
              </div>
           </div>
        </div>
        <div class="modal_page-grid">
            ${state.sectionOpen
              .map((product, i) => {
                return `
                    <div class="modal_page-item">                
                        <div class="modal_page-itemImg" style="background-image: url(${product.imageURL})";></div>
                        <div class="modal_page-itemInfo">
                        <a style="font-weight: bold;">${product.name}</a>
                        <a>${product.description}</a>
                        <a style="margin-top: 5%; margin-top: 5%; ">£${product.price}</a>
                        <button style="font-weight: bold;" class="modal_page-itemButton" onclick="addToBasket(${i})">Add to Basket</button>
                        </div>
                    </div>
                        `;
              })
              .join("")}
        </div>             

      </div>
        
    </div>
  </div>    
    `;

    const sectionEl = document.getElementById("sectionON");
    sectionEl.innerHTML = sectionOn;
  } else {
    const sectionEl = document.getElementById("sectionON");
    sectionEl.innerHTML = "";
  }
}

function displayProducts(index) {
  currentSectionNo = index
  currentSection = data[index];
  state.sectionOpen = currentSection.products;

  sortProductsAZ();
}
function refreshProducts(){
  const sectionEl = document.getElementById("sectionON");
  sectionEl.innerHTML = "";
}

function sortProductsAZ(){
   state.sectionOpen.sort((a,b) => (a.name > b.name) ? 1 :((b.name > a.name) ? -1 :0))
   render()
}
function sortProductsZA(){
    state.sectionOpen.sort((a,b) => (a.name > b.name) ? -1 :((b.name > a.name) ? 1 :0))
    render()
}
function sortProductsDW(){  
  state.sectionOpen.sort((a,b) => (a.price > b.price) ? -1 :((b.price > a.price) ? 1 :0))
  render()
}
function sortProductsUP(){ 
  state.sectionOpen.sort((a,b) => (a.price > b.price) ? 1 :((b.price > a.price) ? -1 :0))
  render()
}


function displayBasket() {
  state.basketOpen = basket.products;
  render();
}
function closeBasket() {
  state.basketOpen = null;
  render();
}
function closeAll() {
  closeBasket();
  state.sectionOpen = null;
  render();
}
function addToBasket(index) {
  currentProduct = currentSection.products[index]; 
  let id = currentProduct.id
  const mapping = `/baskets/${userID}/products/${id}`

  fetch(mapping, {
    method: 'POST',
    headers: {
        "content-Type": "application/json"
    },

  }).then(res => res.json()).then(getBasket).then(render).then(console.log(basket)).catch(console.error);
  
}
function removeFromBasket(index){
 
 

  const mapping = `/baskets/${userID}/products/${index}`
  
  fetch(mapping, {
    method: 'DELETE',
   
  }).then(getBasket).then(closeBasket).then(displayBasket).then(render).then(console.log(basket)).catch(console.error);
  
}

 function checkout() {
  const mapping = `/baskets/${userID}/products/clear`

  fetch(mapping, {
    method: 'DELETE',
    
  }).then(getBasket).then(render).then(console.log(basket)).catch(console.error);
  
 }



let slideIndex = 0;
showSlides();

function showSlides() {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 5000);
}

function confirm(){
   const modalElConfirmButt = 
      `
      <section class="modal-bg" >
          <section class="modal-box-confirm">
              <div style="margin-top: 30%; margin-bottom: 10%;">
                  <div>Checkout Complete</div>
                  <button style="justify-content: centre; margin-left: 40%;" onclick="closeConfirm();">OK</button>
              </div>
          </section>
      </section>
      `

  const modalElConfirm = document.getElementById('confirm')
  modalElConfirm.innerHTML = modalElConfirmButt
}

function closeConfirm(){
  const modalElConfirm = document.getElementById('confirm')
  modalElConfirm.innerHTML = ""
  closeBasket()
  render()
}