package com.barclays.group3;

import org.springframework.data.jpa.repository.JpaRepository;

interface SectionRepo extends JpaRepository<Section, Integer>{
    
}
