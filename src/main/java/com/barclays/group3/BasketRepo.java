package com.barclays.group3;


import org.springframework.data.jpa.repository.JpaRepository;

public interface BasketRepo extends JpaRepository<Basket, Integer> {
Basket findById(String userId);

}
