package com.barclays.group3;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SectionController {
    private SectionRepo repo;

    SectionController(SectionRepo repo){
        this.repo = repo;
    }

    @GetMapping("/sections")
    public List<Section> getProduct(){
        return repo.findAll();
    }

    @GetMapping("/sections/{section_id}")
    public Section getOne(@PathVariable Integer section_id){
        return repo.findById(section_id).get();
    }
    @PostMapping("/sections")
    public Section createSection(@RequestBody Section newSection){
        return repo.save(newSection);
    }
    @PutMapping("/sections/{section_id}")
    public Section updateOne(@PathVariable Integer section_id, @RequestBody Section updatedSection){
        return repo.findById(section_id)
        .map(section -> {
            section.setName(updatedSection.getName());
            section.setImageURL(updatedSection.getImageURL());
            return repo.save(section);
        }).orElseThrow();
    }
    @DeleteMapping("/sections/{section_id}")
    public void deleteOne(@PathVariable Integer section_id){
        repo.deleteById(section_id);
    }

    
}
