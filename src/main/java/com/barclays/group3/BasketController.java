package com.barclays.group3;

import java.util.List;


import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BasketController {
    private BasketRepo repository;
    public BasketController(BasketRepo repository){
        this.repository = repository;
    }

    //TestMapping request//
    @GetMapping("/test")
    public @ResponseBody String greeting() {
        return "Hello, test passed";
    }

    @GetMapping("/baskets")
    public List<Basket> getBaskets(){
        return this.repository.findAll();
    }

    @GetMapping("/baskets/{basket_id}/products/{product_id}")
    public Basket getOne(@PathVariable Integer productId){
        return repository.findById(productId).get();
    }

    @GetMapping("/baskets/{basket_id}")
    public Basket getBasket(@PathVariable String basket_id){
        return repository.findById(basket_id);
         
    }

    @PostMapping("/baskets")
        public Basket createBasket(@RequestBody Basket newBasket){
            return repository.save(newBasket);
    }

    @DeleteMapping("/baskets/{basket_id}/products/{product_array_location}")
    public void deleteOne(@PathVariable String basket_id, @PathVariable int product_array_location){
        Basket basket = repository.findById(basket_id);
        basket.removeProduct(product_array_location);
        repository.save(basket);
    }

    @DeleteMapping("/baskets/{basket_id}/products/clear")
    public void deleteAll(@PathVariable String basket_id){
        Basket basket = repository.findById(basket_id);
        basket.removeAllProducts();
        repository.save(basket);
    }

}

